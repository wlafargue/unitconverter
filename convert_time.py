#!/usr/bin/env python
# coding: utf-8

from dash import callback, html, dcc, callback_context
from dash.dependencies import Input, Output

def block_time():
    return html.Div(children=[
                'Secondes (sec)', html.Br(), dcc.Input(id="secondes", value=None,type="number"),html.Br(),
                'Minutes (min)', html.Br(), dcc.Input(id="minutes", value=None,type="number"),html.Br(),
                'Hours (h)', html.Br(), dcc.Input(id="hours", value=None,type="number"),html.Br(),
                'Days (d)', html.Br(), dcc.Input(id="days", value=None,type="number"),html.Br(),
            ])

@callback(
    Output("secondes", "value"),
    Output("minutes", "value"),
    Output("hours", "value"),
    Output("days", "value"),
    Input("secondes", "value"),
    Input("minutes", "value"),
    Input("hours", "value"),
    Input("days", "value"),
)
def time_converter(secondes, minutes, hours, days):
    ctx = callback_context
    unit = ctx.triggered[0]["prop_id"].split(".")[0]
    if unit == "secondes":
        minutes = None if secondes is None else float(secondes)/60
        hours = None if secondes is None else minutes/60
        days = None if secondes is None else hours/24
    elif unit == "minutes":
        secondes = None if minutes is None else float(minutes)*60
        hours = None if secondes is None else secondes/3600
        days = None if secondes is None else hours/24
    elif unit == "hours":
        secondes = None if hours is None else float(hours)*3600
        minutes = None if secondes is None else secondes/60
        days = None if secondes is None else minutes/1440
    elif unit == "days":
        secondes = None if days is None else float(days)*3600*24
        minutes = None if days is None else secondes/60
        hours = None if days is None else minutes/60
    return secondes, minutes, hours, days
