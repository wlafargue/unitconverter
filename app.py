#!/usr/bin/env python
# coding: utf-8

from dash import html, Dash
from convert_time import block_time
from convert_length import block_length
from convert_temperature import block_temperature
from convert_energy import block_energy
from convert_pressure import block_pressure
import dash_bootstrap_components as dbc

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css',
                        dbc.themes.BOOTSTRAP]

app = Dash(__name__, external_stylesheets=external_stylesheets)
app.title = "Unit Converter"
server = app.server

app.layout = html.Div([
    html.H1('Unit Converter'),
    dbc.Row([
        dbc.Col(children=[
            html.H2('Length'),
            block_length()
        ]),
        dbc.Col(children=[
            html.H2('Temperature'),
            block_temperature(),
            html.Br(),html.Br(),
            html.H2('Pressure'),
            block_pressure()
        ]),
        dbc.Col(children=[
            html.H2('Time'),
            block_time()
        ]),
        dbc.Col(children=[
            html.H2('Energy'),
            block_energy()
        ]),
    ])
], style={'padding': '20px 20px 20px 20px'})

if __name__ == '__main__':
    app.run_server(debug=True)
