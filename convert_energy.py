#!/usr/bin/env python
# coding: utf-8

from dash import callback, html, dcc, callback_context
from dash.dependencies import Input, Output
import dash_latex as dl
from scipy.constants import physical_constants as pc
from scipy.constants import N_A, calorie

def block_energy():
    return html.Div(children=[
        'Hartree (Ha)', html.Br(), dcc.Input(id="hartree", value=None,type="number"),html.Br(),
        'Electronvolt (eV)', html.Br(), dcc.Input(id="electronvolt", value=None,type="number"),html.Br(),
        'Temperature (K)', html.Br(), dcc.Input(id="temperature", value=None,type="number"),html.Br(),
        dl.DashLatex(r'Wavenumber (cm$^{-1}$)'), html.Br(), dcc.Input(id="wavenumber", value=None,type="number"),html.Br(),
        'Wavelength (nm)', html.Br(), dcc.Input(id="wavelength", value=None,type="number"),html.Br(),
        'kcal/mol', html.Br(), dcc.Input(id="kcalmol", value=None,type="number"),html.Br(),
        'kJ/mol', html.Br(), dcc.Input(id="kjoulemol", value=None,type="number"),html.Br(),
        ])

@callback(
    Output("hartree", "value"),
    Output("electronvolt", "value"),
    Output("temperature", "value"),
    Output("wavenumber", "value"),
    Output("wavelength", "value"),
    Output("kcalmol", "value"),
    Output("kjoulemol", "value"),
    Input("hartree", "value"),
    Input("electronvolt", "value"),
    Input("temperature", "value"),
    Input("wavenumber", "value"),
    Input("wavelength", "value"),
    Input("kcalmol", "value"),
    Input("kjoulemol", "value"),
)
def energy_converter(hartree, electronvolt, temperature, wavenumber, \
    wavelength, kcalmol, kjoulemol):
    ctx = callback_context
    unit = ctx.triggered[0]["prop_id"].split(".")[0]
    if unit == "hartree":
        electronvolt = None if hartree is None else pc['Hartree energy in eV'][0]*float(hartree)
        temperature = None if hartree is None else pc['hartree-kelvin relationship'][0]*float(hartree)
        wavenumber = None if hartree is None else pc['hartree-inverse meter relationship'][0]*float(hartree)*1e-2
        wavelength = None if hartree is None else 1e+7/wavenumber
        kcalmol = None if hartree is None else pc['Hartree energy'][0]*float(hartree)/(1000*calorie)*N_A
        kjoulemol = None if hartree is None else pc['Hartree energy'][0]*float(hartree)/1000*N_A
    elif unit == "electronvolt":
        hartree = None if electronvolt is None else pc['electron volt-hartree relationship'][0]*float(electronvolt)
        temperature = None if electronvolt is None else pc['hartree-kelvin relationship'][0]*hartree
        wavenumber = None if electronvolt is None else pc['hartree-inverse meter relationship'][0]*hartree*1e-2
        wavelength = None if electronvolt is None else 1e+7/wavenumber
        kcalmol = None if electronvolt is None else pc['Hartree energy'][0]*hartree/(1000*calorie)*N_A
        kjoulemol = None if electronvolt is None else pc['Hartree energy'][0]*hartree/1000*N_A
    elif unit == "temperature":
        hartree = None if temperature is None else float(temperature)/pc['hartree-kelvin relationship'][0]
        electronvolt = None if temperature is None else pc['Hartree energy in eV'][0]*hartree
        wavenumber = None if temperature is None else pc['hartree-inverse meter relationship'][0]*hartree*1e-2
        wavelength = None if temperature is None else 1e+7/wavenumber
        kcalmol = None if temperature is None else pc['Hartree energy'][0]*hartree/(1000*calorie)*N_A
        kjoulemol = None if temperature is None else pc['Hartree energy'][0]*hartree/1000*N_A
    elif unit == "wavenumber":
        hartree = None if wavenumber is None else float(wavenumber)*1e+2/pc['hartree-inverse meter relationship'][0]
        electronvolt = None if wavenumber is None else pc['Hartree energy in eV'][0]*hartree
        wavelength = None if wavenumber is None else 1e+7/float(wavenumber)
        temperature = None if wavenumber is None else pc['hartree-kelvin relationship'][0]*hartree
        kcalmol = None if wavenumber is None else pc['Hartree energy'][0]*hartree/(1000*calorie)*N_A
        kjoulemol = None if wavenumber is None else pc['Hartree energy'][0]*hartree/1000*N_A
    elif unit == "wavelength":
        wavenumber = None if wavelength is None else 1e+7/float(wavelength)
        hartree = None if wavelength is None else wavenumber*1e+2/pc['hartree-inverse meter relationship'][0]
        electronvolt = None if wavelength is None else pc['Hartree energy in eV'][0]*hartree
        temperature = None if wavelength is None else pc['hartree-kelvin relationship'][0]*hartree
        kcalmol = None if wavelength is None else pc['Hartree energy'][0]*hartree/(1000*calorie)*N_A
        kjoulemol = None if wavelength is None else pc['Hartree energy'][0]*hartree/1000*N_A
    elif unit == "kcalmol":
        kjoulemol = None if kcalmol is None else float(kcalmol)*calorie
        hartree = None if kcalmol is None else kjoulemol*1e+3/(N_A*pc['Hartree energy'][0])
        electronvolt = None if kcalmol is None else pc['Hartree energy in eV'][0]*hartree
        temperature = None if kcalmol is None else pc['hartree-kelvin relationship'][0]*hartree
        wavenumber = None if kcalmol is None else pc['hartree-inverse meter relationship'][0]*hartree*1e-2
        wavelength = None if kcalmol is None else 1e+7/wavenumber
    elif unit == "kjoulemol":
        kcalmol = None if kjoulemol is None else float(kjoulemol)/calorie
        hartree = None if kjoulemol is None else kjoulemol*1e+3/(N_A*pc['Hartree energy'][0])
        electronvolt = None if kjoulemol is None else pc['Hartree energy in eV'][0]*hartree
        temperature = None if kjoulemol is None else pc['hartree-kelvin relationship'][0]*hartree
        wavenumber = None if kjoulemol is None else pc['hartree-inverse meter relationship'][0]*hartree*1e-2
        wavelength = None if kjoulemol is None else 1e+7/wavenumber
    return hartree, electronvolt, temperature, wavenumber, wavelength, kcalmol, kjoulemol

