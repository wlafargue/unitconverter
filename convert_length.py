#!/usr/bin/env python
# coding: utf-8

from dash import callback, html, dcc, callback_context
from dash.dependencies import Input, Output
import dash_latex as dl
from scipy.constants import physical_constants as pc
from scipy.constants import inch, yard

def block_length():
    return html.Div(children=[
        'Meter (m)', html.Br(), dcc.Input(id="meter", value=None,type="number"),html.Br(),
        dl.DashLatex(r'Angström ($\AA$)'), html.Br(), dcc.Input(id="angstrom", value=None,type="number"),html.Br(),
        dl.DashLatex(r'Bohr ($a_0$)'), html.Br(), dcc.Input(id="bohr", value=None,type="number"),html.Br(),
        'Inch (in)', html.Br(), dcc.Input(id="inches", value=None,type="number"),html.Br(),
        'Yard (yard)', html.Br(), dcc.Input(id="yards", value=None,type="number"),html.Br(),
        ])

@callback(
    Output("meter", "value"),
    Output("angstrom", "value"),
    Output("bohr", "value"),
    Output("inches", "value"),
    Output("yards", "value"),
    Input("meter", "value"),
    Input("angstrom", "value"),
    Input("bohr", "value"),
    Input("inches", "value"),
    Input("yards", "value"),
)
def length_converter(meter, angstrom, bohr, inches, yards):
    ctx = callback_context
    unit = ctx.triggered[0]["prop_id"].split(".")[0]
    if unit == "meter":
        angstrom = None if meter is None else float(meter)*1e+10
        bohr = None if meter is None else float(meter)/pc['Bohr radius'][0]
        inches = None if meter is None else float(meter)/inch
        yards = None if meter is None else float(meter)/yard
    elif unit == "angstrom":
        meter = None if angstrom is None else float(angstrom)*1e-10
        bohr = None if angstrom is None else meter/pc['Bohr radius'][0]
        inches = None if angstrom is None else meter/inch
        yards = None if angstrom is None else meter/yard
    elif unit == "bohr":
        meter = None if bohr is None else float(bohr)*pc['Bohr radius'][0]
        angstrom = None if bohr is None else meter*1e+10
        inches = None if bohr is None else meter/inch
        yards = None if bohr is None else meter/yard
    elif unit == "inches":
        meter = None if inches is None else float(inches)*inch
        angstrom = None if inches is None else meter*1e+10
        bohr = None if inches is None else meter/pc['Bohr radius'][0]
        yards = None if inches is None else meter/yard
    elif unit == "yards":
        meter = None if yards is None else float(yards)*yard
        angstrom = None if yards is None else meter*1e+10
        bohr = None if yards is None else meter/pc['Bohr radius'][0]
        inches = None if bohr is None else meter/inch
    return meter, angstrom, bohr, inches, yards

