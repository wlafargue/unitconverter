#!/usr/bin/env python
# coding: utf-8

from dash import callback, html, dcc, callback_context
from dash.dependencies import Input, Output
from scipy.constants import atm, bar, mmHg

def block_pressure():
    return html.Div(children=[
        'Pascal (Pa)', html.Br(), dcc.Input(id="pascal", value=None,type="number"),html.Br(),
        'Atmosphere (atm)', html.Br(), dcc.Input(id="atmosphere", value=None, type="number"),html.Br(),
        'Bar (bar)', html.Br(), dcc.Input(id="bar_u", value=None, type="number"),html.Br(),
        'mmHg or Torr', html.Br(), dcc.Input(id="mm_Hg", value=None, type="number"),html.Br(),
        r'N/m$^{2}$', html.Br(), dcc.Input(id="Nmsq", value=None, type="number"),
        ])

@callback(
    Output("pascal", "value"),
    Output("atmosphere", "value"),
    Output("bar_u", "value"),
    Output("mm_Hg", "value"),
    Output("Nmsq", "value"),
    Input("pascal", "value"),
    Input("atmosphere", "value"),
    Input("bar_u", "value"),
    Input("mm_Hg", "value"),
    Input("Nmsq", "value"),
)
def pressure_converter(pascal, atmosphere, bar_u, mm_Hg, Nmsq):
    ctx = callback_context
    unit = ctx.triggered[0]["prop_id"].split(".")[0]
    if unit == "pascal":
        atmosphere = None if pascal is None else float(pascal)/atm
        bar_u = None if pascal is None else float(pascal)/bar
        mm_Hg = None if pascal is None else float(pascal)/mmHg
        Nmsq = None if pascal is None else float(pascal)
    elif unit == "atmosphere":
        pascal = None if atmosphere is None else float(atmosphere)*atm
        bar_u = None if atmosphere is None else pascal/bar
        mm_Hg = None if atmosphere is None else pascal/mmHg
        Nmsq = None if atmosphere is None else pascal
    elif unit == "bar_u":
        pascal = None if bar_u is None else float(bar_u)*bar
        atmosphere = None if bar_u is None else pascal/atm
        mm_Hg = None if bar_u is None else pascal/mmHg
        Nmsq = None if bar_u is None else pascal
    elif unit == "mm_Hg":
        pascal = None if mm_Hg is None else float(mm_Hg)*mmHg
        atmosphere = None if mm_Hg is None else pascal/atm
        bar_u = None if mm_Hg is None else pascal/bar
        Nmsq = None if mm_Hg is None else pascal
    elif unit == "Nmsq":
        pascal = None if Nmsq is None else float(Nmsq)
        atmosphere = None if pascal is None else pascal/atm
        bar_u = None if pascal is None else pascal/bar
        mm_Hg = None if pascal is None else pascal/mmHg
    return pascal, atmosphere, bar_u, mm_Hg, Nmsq

